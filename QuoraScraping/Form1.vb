﻿Imports PuppeteerSharp
Imports HtmlAgilityPack
Imports System.Text.RegularExpressions
Imports System.IO

Public Class Form1

    Dim LastQuestionLink = ""

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            MainURLScraping()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Async Sub MainURLScraping()
        Try
            Await New BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision)
            Dim browser = Await Puppeteer.LaunchAsync(New LaunchOptions With {
                .Headless = False
            })
            Dim page = Await browser.NewPageAsync()
            'Await page.SetViewportAsync(New ViewPortOptions With {
            '    .Width = 1280,
            '    .Height = 926})
            Await page.GoToAsync("https://www.quora.com/search?q=how%20many")

            Dim Pagesize = page.Viewport.Height
            While page.Viewport.Height > 0
                Await page.EvaluateFunctionAsync(" () => window.scrollBy(0," + Convert.ToInt32(Pagesize).ToString() + ")")
                Await page.WaitForExpressionAsync("document.body.scrollHeight > " + Pagesize.ToString())
                Await page.WaitForTimeoutAsync(10000)
                Dim result = Await page.GetContentAsync()

                If LastQuestionLink <> "" Then
                    Dim myDelims As String() = New String() {LastQuestionLink}
                    result = result.Split(myDelims, StringSplitOptions.None)(1)
                End If

                Await ScrapQuestionHrefLinks(result, browser)
                'Await page.CloseAsync()
                Pagesize = Pagesize + 200
            End While
        Catch ex As Exception
            Throw ex
        End Try
        'Dim result2 = Await page.GetContentAsync()
    End Sub

    Private Async Function ScrapQuestionHrefLinks(ByVal text As String, ByVal browser As Browser) As Task
        Try
            Dim pattern = "(\/How-many(?:.*?)\ target)"
            Dim AllLinks = System.Text.RegularExpressions.Regex.Split(text, pattern).Where(Function(x, i) i Mod 2 <> 0).ToArray()

            If AllLinks.Count > 0 Then
                LastQuestionLink = AllLinks(AllLinks.Count - 1).ToString()
                For Each Link In AllLinks
                    Link = Link.Remove(Link.Length - 8, 8)
                    Dim QuestionLink = "https://www.quora.com" + Link
                    Await QuestionAnsScraping(QuestionLink, Link, browser)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Async Function QuestionAnsScraping(ByVal QuestionLink As String, ByVal txtQuestion As String, ByVal browser As Browser) As Task
        Try
            Dim Newpage = Await browser.NewPageAsync()
            Await Newpage.GoToAsync(QuestionLink)
            Dim Question = Await Newpage.EvaluateFunctionAsync(Of String)(
                    "() => document.querySelector('.ui_qtext_rendered_qtext').innerHTML ")
            Dim Answers = Await Newpage.EvaluateFunctionAsync(Of String())(
                    "() => Array.from(document.querySelectorAll('.ui_qtext_expanded')).map(a => a.innerHTML)")

            Dim Pagesize = 900
            While Answers.Count < 3
                Await Newpage.EvaluateFunctionAsync(" () => window.scrollBy(0," + Convert.ToInt32(Pagesize).ToString() + ")")
                Await Newpage.WaitForExpressionAsync("document.body.scrollHeight > " + Pagesize.ToString())
                Await Newpage.WaitForTimeoutAsync(5000)
                Answers = Await Newpage.EvaluateFunctionAsync(Of String())(
                    "() => Array.from(document.querySelectorAll('.ui_qtext_expanded')).map(a => a.innerHTML)")
                Pagesize = Pagesize + 200
            End While

            Dim ToDate As String = String.Format("{0:ddMMyyyy}", DateTime.Now)
            Dim CurrentPath = Environment.CurrentDirectory + "/QuestionAnswers/" + ToDate
            If Not Directory.Exists(CurrentPath) Then
                Directory.CreateDirectory(CurrentPath)
            End If
            Dim rgx As Regex = New Regex("[^a-zA-Z0-9 -]")
            txtQuestion = rgx.Replace(txtQuestion, "")
            If txtQuestion.Length > 160 Then
                txtQuestion = txtQuestion.ToString().Substring(0, 150)
            End If

            If Not File.Exists(CurrentPath + "/" + txtQuestion + ".txt") Then
                File.WriteAllText(CurrentPath + "/" + txtQuestion + ".txt", String.Empty)
            End If

            Dim txt As TextWriter = New StreamWriter(CurrentPath + "/" + txtQuestion + ".txt")
            txt.Write("Question : " + Question.ToString())
            txt.Write(Environment.NewLine)
            txt.Write(Environment.NewLine)
            txt.Write("Answers : -")
            txt.Write(Environment.NewLine)
            txt.Write(Environment.NewLine)

            For index As Integer = 0 To 2
                Dim Answer = Answers(index)
                txt.Write((index + 1).ToString() + " : -")
                txt.Write(Environment.NewLine)
                Dim pattern As String = "<p.*?>(.*?)<\/p>"
                Dim matches As MatchCollection = Regex.Matches(Answer, pattern)

                Dim RemovedTagString = StripTagsRegex(Answer)
                'Dim Testcount = matches.Count
                For Each m In matches
                    Dim pFrom As Integer = m.ToString().IndexOf(">") + ">".Length
                    Dim pTo As Integer = m.ToString().LastIndexOf("<")
                    Dim Ptag As String = m.ToString().Substring(pFrom, pTo - pFrom)
                    txt.Write(Ptag.ToString())
                    txt.Write(Environment.NewLine)
                Next
                'txt.Write(RemovedTagString.ToString())
                txt.Write(Environment.NewLine)
                txt.Write(Environment.NewLine)
            Next
            txt.Close()
            Await Newpage.CloseAsync()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function StripTagsRegex(ByVal source As String) As String
        Dim ReplacedString = Regex.Replace(source, "(\<(\/)?(\w)*(\d)?\>)", String.Empty)
        ReplacedString = Regex.Replace(source, "<.*?>", String.Empty)
        Return ReplacedString
    End Function

End Class
